DESCRIPTION = "Commented config.txt file for the Raspberry Pi. \
               The Raspberry Pi config.txt file is read by the GPU before \
               the ARM core is initialised. It can be used to set various \
               system configuration parameters."

VC4GRAPHICS = "0"
ENABLE_UART = "1"

RPI_EXTRA_CONFIG = '\n'
RPI_EXTRA_CONFIG += '## meta-rpi-guitarpedal customisations\n'
RPI_EXTRA_CONFIG += '\n'
RPI_EXTRA_CONFIG += '# startup options\n'
RPI_EXTRA_CONFIG += 'disable_splash=1\n'
RPI_EXTRA_CONFIG += '\n'
RPI_EXTRA_CONFIG += '# audio hardware\n'
RPI_EXTRA_CONFIG += 'dtparam=audio=off\n'
RPI_EXTRA_CONFIG += 'dtoverlay=audioinjector-ultra\n'
RPI_EXTRA_CONFIG += '\n'
RPI_EXTRA_CONFIG += '# waveshare 5 inch TFT + touchscreen\n'
#RPI_EXTRA_CONFIG += 'framebuffer_width=800\n'
#RPI_EXTRA_CONFIG += 'framebuffer_height=480\n'
RPI_EXTRA_CONFIG += 'dtparam=i2c_arm=on\n'
RPI_EXTRA_CONFIG += 'dtparam=spi=on\n'
RPI_EXTRA_CONFIG += 'max_usb_current=1\n'
RPI_EXTRA_CONFIG += 'hdmi_group=2\n'
RPI_EXTRA_CONFIG += 'hdmi_mode=87\n'
RPI_EXTRA_CONFIG += 'hdmi_cvt 800 480 60 6 0 0 0\n'
RPI_EXTRA_CONFIG += 'hdmi_drive=1\n'
RPI_EXTRA_CONFIG += 'hdmi_force_hotplug=1\n'
RPI_EXTRA_CONFIG += 'dtoverlay=ads7846,cs=1,penirq=25,penirq_pull=2,speed=50000,keep_vref_on=0,swapxy=0,pmax=255,xohms=150,xmin=200,xmax=3900,ymin=200,ymax=3900'
# RPI_EXTRA_CONFIG += 'display_rotate=.?.\n'
