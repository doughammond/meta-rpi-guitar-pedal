require recipes-core/images/core-image-minimal.bb

#BOOT_SPACE = "256000"
SDIMG_KERNELIMAGE_raspberrypi4-64 ?= "kernel8.img"

IMAGE_FSTYPES = "tar.bz2 ext3 rpi-sdimg wic.bz2 wic.bmap"

IMAGE_INSTALL += "libstdc++ mtd-utils"
IMAGE_INSTALL += "pulseaudio"
